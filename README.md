ARTIFACTS :

https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsload_performance

BUILD & PUSH:


```bash
export IMAGE="enzo13/api-pipeline-model" && \
docker build -t $IMAGE:latest -t $IMAGE:1.0 services/api-pipeline && \
docker push --all-tags $IMAGE
```

Services list :

